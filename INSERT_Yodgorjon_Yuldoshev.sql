-- Inserting the favorite film into the "film" table
INSERT INTO film (title, description, release_year, language_id, rental_duration, rental_rate, length, replacement_cost, rating, special_features)
VALUES ('The Shawshank Redemption', 'Two imprisoned men bond over a number of years, finding solace and eventual redemption through acts of common decency.', 1994, 1, 14, 4.99, 142, 19.99, 'R', ARRAY['Deleted Scenes', 'Behind the Scenes']);

-- Adding actors to the "actor" table
INSERT INTO actor (first_name, last_name)
VALUES 
  ('Tim', 'Robbins'),
  ('Morgan', 'Freeman'),
  ('Bob', 'Gunton');

-- Linking actors to the film in the "film_actor" table
INSERT INTO film_actor (film_id, actor_id)
VALUES 
  ((SELECT film_id FROM film WHERE title = 'The Shawshank Redemption'),
  (SELECT actor_id FROM actor WHERE first_name = 'Tim' AND last_name = 'Robbins')),
  
  ((SELECT film_id FROM film WHERE title = 'The Shawshank Redemption'),
  (SELECT actor_id FROM actor WHERE first_name = 'Morgan' AND last_name = 'Freeman')),
  
  ((SELECT film_id FROM film WHERE title = 'The Shawshank Redemption'),
  (SELECT actor_id FROM actor WHERE first_name = 'Bob' AND last_name = 'Gunton'));

-- Adding the film to a store's inventory (replace your_store_id_here with the actual store ID)
INSERT INTO inventory (film_id, store_id)
VALUES 
  ((SELECT film_id FROM film WHERE title = 'The Shawshank Redemption'),
  1);
