--Create a view called "sales_revenue_by_category_qtr"
CREATE OR REPLACE VIEW sales_revenue_by_category_qtr AS
SELECT c.name AS category_name, SUM(p.amount) AS total_revenue
FROM category c
JOIN film_category fc ON c.category_id = fc.category_id
JOIN film f ON fc.film_id = f.film_id
JOIN inventory i ON f.film_id = i.film_id
JOIN rental r ON i.inventory_id = r.inventory_id
JOIN payment p ON r.rental_id = p.rental_id
WHERE EXTRACT(QUARTER FROM r.rental_date) = EXTRACT(QUARTER FROM CURRENT_DATE)
AND EXTRACT(YEAR FROM r.rental_date) = EXTRACT(YEAR FROM CURRENT_DATE)
GROUP BY c.name
HAVING SUM(p.amount) > 0;

--Create a query language function called "get_sales_revenue_by_category_qtr"
CREATE OR REPLACE FUNCTION get_sales_revenue_by_category_qtr(current_quarter INT)
RETURNS TABLE(category_name TEXT, total_revenue NUMERIC) AS $$
BEGIN
    RETURN QUERY
    SELECT c.name AS category_name, SUM(p.amount) AS total_revenue
    FROM category c
    JOIN film_category fc ON c.category_id = fc.category_id
    JOIN film f ON fc.film_id = f.film_id
    JOIN inventory i ON f.film_id = i.film_id
    JOIN rental r ON i.inventory_id = r.inventory_id
    JOIN payment p ON r.rental_id = p.rental_id
    WHERE EXTRACT(QUARTER FROM r.rental_date) = current_quarter
    AND EXTRACT(YEAR FROM r.rental_date) = EXTRACT(YEAR FROM CURRENT_DATE)
    GROUP BY c.name
    HAVING SUM(p.amount) > 0;
END;
$$ LANGUAGE plpgsql;

--Create a procedure language function called "new_movie"
CREATE OR REPLACE FUNCTION new_movie(movie_title TEXT)
RETURNS VOID AS $$
DECLARE
    lang_id INT;
BEGIN
    -- Check if language exists
    SELECT language_id INTO lang_id
    FROM language
    WHERE name = 'Klingon';

    IF NOT FOUND THEN
        RAISE EXCEPTION 'Language "Klingon" does not exist in the language table.';
    END IF;

    -- Insert new movie
    INSERT INTO film (title, rental_rate, rental_duration, replacement_cost, release_year, language_id)
    VALUES (movie_title, 4.99, 3, 19.99, EXTRACT(YEAR FROM CURRENT_DATE), lang_id);
END;
$$ LANGUAGE plpgsql;
