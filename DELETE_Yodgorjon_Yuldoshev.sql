-- Delete rental records related to the film
DELETE FROM rental
WHERE inventory_id IN (
    SELECT inventory_id
    FROM inventory
    WHERE film_id = 1001
);

-- Remove the film from the inventory
DELETE FROM inventory
WHERE film_id = 1001;

-- Removing records related to me/customer_id 1 
DELETE FROM film_actor
WHERE film_id IN (
    SELECT film_id
    FROM rental
    WHERE customer_id = 1
);

DELETE FROM payment
WHERE customer_id = 1;

DELETE FROM rental
WHERE customer_id = 1;



