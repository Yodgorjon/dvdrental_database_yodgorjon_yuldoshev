--Step 1: Create a new user with limited permissions:
CREATE USER rentaluser WITH PASSWORD 'rentalpassword';

--Step 2: Grant SELECT permission for the "customer" table to "rentaluser":
GRANT SELECT ON TABLE customer TO rentaluser;

--Step 3: Test the SELECT permission for "rentaluser":
-- Login with rentaluser and execute:
SELECT * FROM customer;

--Step 4: Create a new user group "rental" and add "rentaluser" to the group:
CREATE GROUP rental;
GRANT rental TO rentaluser;

--Step 5: Grant INSERT and UPDATE permissions for the "rental" table to the "rental" group:
GRANT INSERT, UPDATE ON TABLE rental TO rental;

--Step 6: Insert and update data in the "rental" table using the "rental" group role:
-- Login with rentaluser and execute:
INSERT INTO rental (rental_date, inventory_id, customer_id, return_date, staff_id)
VALUES ('2024-04-07', 1, 1, '2024-04-08', 1);

-- Login with rentaluser and execute:
UPDATE rental SET return_date = '2024-04-09' WHERE rental_id = 1;

--Step 7: Revoke INSERT permission for the "rental" table from the "rental" group:
REVOKE INSERT ON TABLE rental FROM rental;

--Step 8: Attempt to insert data into the "rental" table after revoking permission (fail):
-- Login with rentaluser and execute (should result in permission denied error):
INSERT INTO rental (rental_date, inventory_id, customer_id, return_date, staff_id)
VALUES ('2024-04-07', 1, 1, '2024-04-08', 1);

--Step 9: Create a personalized role for existing customers with payment and rental history:
CREATE ROLE client_John_Doe;
GRANT USAGE ON SCHEMA public TO client_John_Doe;
GRANT SELECT ON TABLE rental TO client_John_Doe;
GRANT SELECT ON TABLE payment TO client_John_Doe;
ALTER DEFAULT PRIVILEGES FOR ROLE client_John_Doe IN SCHEMA public
GRANT SELECT ON TABLES TO client_John_Doe;

--Step 10: Test the personalized role to ensure it can access only specific data:
-- Login with the client_John_Doe role and execute:
SELECT * FROM rental WHERE customer_id = (SELECT customer_id FROM customer WHERE first_name = 'John' AND last_name = 'Doe');
SELECT * FROM payment WHERE customer_id = (SELECT customer_id FROM customer WHERE first_name = 'John' AND last_name = 'Doe');

