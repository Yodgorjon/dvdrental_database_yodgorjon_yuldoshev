-- Alter Rental Duration and Rental Rate of the Film with film_id = 1001
UPDATE film
SET rental_duration = 21, -- Three weeks (3 weeks * 7 days = 21 days)
    rental_rate = 9.99
WHERE film_id = 1001;

-- Update Customer Data for customer_id = 1
UPDATE customer
SET first_name = 'Yodgorjon',
    last_name = 'Yuldoshev',
    email = 'yodgorjonyuldoshev@gmail.com',
    address_id = 1, 
    create_date = current_date
WHERE customer_id = 1;
